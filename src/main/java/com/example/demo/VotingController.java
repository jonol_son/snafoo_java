package com.example.demo;

import com.example.demo.models.Snack;
import com.example.demo.models.User;
import com.example.demo.service.SnackAPIService;
import com.example.demo.service.SnackService;
import com.example.demo.service.UserService;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class VotingController {

  private static final String VISITOR = "visitor";
  private static final String API_EXCEPTION = "apiException";
  private static final String SUCCESS = "success";
  private static final String VIEW_NAME = "voting";
  private static final String SUGGESTED_SNACKS = "suggestedSnacks";
  private static final String ALWAYS_PURCHASED = "alwaysPurchased";
  private static final String EXISTING_VOTES = "existingVotes";
  private static final String USER_ID = "userId";
  private static final String REMAINING_VOTES = "remainingVotes";
  private static final String SNACK_NAME = "snackName";
  @Autowired
  private UserService userService;
  @Autowired
  private SnackService snackService;

  @Autowired
  private SnackAPIService snackApiService;

  @GetMapping(value = {"", "/", "/voting"})
  public ModelAndView voting(HttpServletResponse response,
                             @CookieValue(value = VISITOR, required = false) String uuid) {
    User currentUser = userService.getOrCreateUser(uuid);
    response.addCookie(userService.getUserCookie(currentUser));

    ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
    List<Integer> existingVotes = userService.getUsersExistingVotes(currentUser);

    try {
      snackService.syncSnacks();
      modelAndView.addObject(API_EXCEPTION, false);
    } catch (Exception e) {
      modelAndView.addObject(API_EXCEPTION, true);
    }
    modelAndView.addObject(SUGGESTED_SNACKS, snackService.getSuggestedSnacks());
    modelAndView.addObject(ALWAYS_PURCHASED, snackService.getAlwaysPurchasedSnacks());
    modelAndView.addObject(EXISTING_VOTES, existingVotes);
    modelAndView.addObject(USER_ID, currentUser.getId());
    modelAndView.addObject(REMAINING_VOTES, 3 - existingVotes.size());
    return modelAndView;
  }

  @PostMapping(value = {"", "/", "/voting"})
  public @ResponseBody
  String vote(@RequestParam(name = SNACK_NAME, required = true) String name,
              @RequestParam(name = USER_ID, required = true) Integer userId) {
    User currentUser = userService.getUserById(userId);
    Snack snackToVoteFor = snackService.findSnackByName(name);
    snackService.userVotesForSnack(currentUser, snackToVoteFor);
    return SUCCESS;
  }
}