package com.example.demo;

import com.example.demo.error.SnackHasAlreadyBeenSuggested;
import com.example.demo.error.SuggestedNonOptionalSnack;
import com.example.demo.forms.SuggestSnackForm;
import com.example.demo.models.Snack;
import com.example.demo.models.User;
import com.example.demo.service.SnackService;
import com.example.demo.service.UserService;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SuggestionsController {

  private static final String VISITOR = "visitor";
  private static final String PREVIOUSLY_SUGGESTED_SNACKS = "previouslySuggestedSnacks";
  private static final String VIEW_NAME = "suggestions";
  private static final String HAS_ALREADY_VOTED = "hasAlreadyVoted";
  private static final String SUGGEST_SNACK_FORM = "suggestSnackForm";
  private static final String ERROR_OCCURRED_WHILE_SAVING = "errorOccurredWhileSaving";
  private static final String ERROR_TEXT = "errorText";
  private static final String API_EXCEPTION = "apiException";

  @Autowired
  private UserService userService;
  @Autowired
  private SnackService snackService;

  @GetMapping("/suggestions")
  public ModelAndView suggestions(HttpServletResponse response,
                                  @CookieValue(value = VISITOR, required = false) String uuid,
                                  SuggestSnackForm suggestSnackForm) {
    User currentUser = userService.getOrCreateUser(uuid);
    response.addCookie(userService.getUserCookie(currentUser));
    ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
    try {
      snackService.syncSnacks();
      modelAndView.addObject(API_EXCEPTION, false);
    } catch (Exception e) {
      modelAndView.addObject(API_EXCEPTION, true);
    }
    modelAndView
        .addObject(PREVIOUSLY_SUGGESTED_SNACKS, snackService.getSnacksEligibleForSuggesting());
    modelAndView.addObject(HAS_ALREADY_VOTED, userService.hasUserSuggested(currentUser));
    modelAndView.addObject(SUGGEST_SNACK_FORM, new SuggestSnackForm());
    modelAndView.addObject(ERROR_OCCURRED_WHILE_SAVING, false);
    return modelAndView;
  }

  @PostMapping("/suggestions")
  public ModelAndView suggestSnack(@Valid SuggestSnackForm suggestSnackForm,
                                   BindingResult bindingResult,
                                   @CookieValue(value = VISITOR, required = false) String uuid) {
    ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
    modelAndView.addObject(HAS_ALREADY_VOTED, false);
    modelAndView
        .addObject(PREVIOUSLY_SUGGESTED_SNACKS, snackService.getSnacksEligibleForSuggesting());
    modelAndView.addObject(API_EXCEPTION, false);
    if (bindingResult.hasErrors()) {
      modelAndView.addObject(ERROR_OCCURRED_WHILE_SAVING, false);
      return modelAndView;
    }

    String errorMessage = "";
    try {
      Snack suggestedSnack = suggestSnackForm.getSnack();
      snackService.suggestSnack(suggestedSnack);
      User currentUser = userService.getOrCreateUser(uuid);
      userService.userSuggestedSnack(currentUser);
      return new ModelAndView("redirect:/voting");

    } catch (SuggestedNonOptionalSnack e) {
      errorMessage = e.getMessage();
    } catch (SnackHasAlreadyBeenSuggested e) {
      errorMessage = e.getMessage();
    } catch (Exception e) {
      errorMessage = "An Error Occurred While saving your suggestion.  Please Try again later";
    }
    modelAndView.addObject(ERROR_OCCURRED_WHILE_SAVING, true);
    modelAndView.addObject(ERROR_TEXT, errorMessage);
    return modelAndView;
  }
}