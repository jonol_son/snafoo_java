package com.example.demo.error;

public class SnackAlreadyExists extends RuntimeException {

  private final String name;

  public SnackAlreadyExists(final String name) {
    super(String.format("Snack with name '%s' already exists.", name));
    this.name = name;
  }

  public String getName() {
    return name;
  }

}