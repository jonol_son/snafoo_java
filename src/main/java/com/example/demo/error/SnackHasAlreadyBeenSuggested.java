package com.example.demo.error;

public class SnackHasAlreadyBeenSuggested extends RuntimeException {

  private final String name;

  public SnackHasAlreadyBeenSuggested(final String name) {
    super(String.format("Snack with name '%s' has already been suggested this month.", name));
    this.name = name;
  }

  public String getName() {
    return name;
  }

}