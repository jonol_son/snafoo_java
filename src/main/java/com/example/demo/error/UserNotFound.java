package com.example.demo.error;

public class UserNotFound extends RuntimeException {

  public UserNotFound() {
    super(String.format("Could not find a user with that Id"));
  }
}