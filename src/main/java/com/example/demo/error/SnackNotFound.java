package com.example.demo.error;

public class SnackNotFound extends RuntimeException {

  private final String name;

  public SnackNotFound(final String name) {
    super(String.format("Could not find snack with name: '%s'.", name));
    this.name = name;
  }

  public String getName() {
    return name;
  }

}