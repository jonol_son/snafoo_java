package com.example.demo.error;

public class SuggestedNonOptionalSnack extends RuntimeException {

  private final String name;

  public SuggestedNonOptionalSnack(final String name) {
    super(String.format("Snack with name '%s' already exists and it is not optional", name));
    this.name = name;
  }

  public String getName() {
    return name;
  }

}