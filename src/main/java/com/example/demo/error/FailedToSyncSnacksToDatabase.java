package com.example.demo.error;

public class FailedToSyncSnacksToDatabase extends RuntimeException {

  public FailedToSyncSnacksToDatabase() {
    super(String.format("Failed to sync snacks from OCD with database"));
  }
}