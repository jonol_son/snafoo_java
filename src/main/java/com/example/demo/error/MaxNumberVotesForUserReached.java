package com.example.demo.error;

public class MaxNumberVotesForUserReached extends RuntimeException {

  public MaxNumberVotesForUserReached() {
    super(String.format("User has already reached the maximum number of votes"));
  }
}