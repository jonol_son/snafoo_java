package com.example.demo.error;

public class SnacksNotFound extends RuntimeException {

  private final String category;

  public SnacksNotFound(final String category) {
    super(String.format("Could not find any snacks filtering by '%s'.", category));
    this.category = category;
  }

  public String getCategory() {
    return category;
  }

}