package com.example.demo.error;

public class UserHasAlreadyVotedForSnack extends RuntimeException {

  private final String name;

  public UserHasAlreadyVotedForSnack(final String name) {
    super(String.format("Snack with name '%s' has already been voted on by this user.", name));
    this.name = name;
  }

  public String getName() {
    return name;
  }

}