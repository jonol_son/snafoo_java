package com.example.demo.error;

import com.example.demo.models.ErrorDBLogger;
import com.example.demo.models.ErrorDBLoggerRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

  @Autowired
  private ErrorDBLoggerRepository errorDBLoggerRepository;

  @ExceptionHandler
  public ResponseEntity<List<String>> handleException(final Exception e) {
    List<String> errorList = new ArrayList<>();

    if (e instanceof SnackAlreadyExists) {
      final SnackAlreadyExists snackAlreadyExists = (SnackAlreadyExists) e;
      errorList.add(
          String.format("Aggregate with name '%s' already exists.", snackAlreadyExists.getName()));
      return ResponseEntity.badRequest().body(errorList);

    } else if (e instanceof SuggestedNonOptionalSnack) {
      final SuggestedNonOptionalSnack suggestedNonOptionalSnack = (SuggestedNonOptionalSnack) e;
      errorList.add(String.format("Snack with name '%s' already exists and it is not optional",
                                  suggestedNonOptionalSnack.getName()
      ));
      return ResponseEntity.badRequest().body(errorList);
    } else if (e instanceof SnackHasAlreadyBeenSuggested) {
      final SnackHasAlreadyBeenSuggested snackHasAlreadyBeenSuggested = (SnackHasAlreadyBeenSuggested) e;
      errorList.add(String.format(
          "Snack with name '%s' has already been suggested this month.",
          snackHasAlreadyBeenSuggested.getName()
      ));
      return ResponseEntity.badRequest().body(errorList);
    } else if (e instanceof SnackNotFound) {
      final SnackNotFound snackNotFound = (SnackNotFound) e;
      errorList
          .add(String.format("Could not find snack with name: '%s'.", snackNotFound.getName()));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorList);
    } else if (e instanceof SnacksNotFound) {
      final SnacksNotFound snacksNotFound = (SnacksNotFound) e;
      errorList.add(String.format("Could not find any snacks filtering by '%s'.",
                                  snacksNotFound.getCategory()
      ));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorList);
    } else if (e instanceof UserHasAlreadyVotedForSnack) {
      final UserHasAlreadyVotedForSnack userHasAlreadyVotedForSnack = (UserHasAlreadyVotedForSnack) e;
      errorList.add(String.format(
          "Snack with name '%s' has already been voted on by this user.",
          userHasAlreadyVotedForSnack.getName()
      ));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorList);
    } else if (e instanceof FailedToSyncSnacksToDatabase) {
      ErrorDBLogger error = new ErrorDBLogger(e);
      errorDBLoggerRepository.save(error);
      errorList.add(String.format("Failed to sync snacks from OCD with database"));
      return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(errorList);
    } else if (e instanceof MaxNumberVotesForUserReached) {
      errorList.add(String.format("You have already voted the maximum number of times"));
      return ResponseEntity.badRequest().body(errorList);
    } else if (e instanceof UserNotFound) {
      errorList.add(String.format("Could not find a user with that userId"));
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorList);
    } else {
      ErrorDBLogger error = new ErrorDBLogger(e);
      errorDBLoggerRepository.save(error);
      errorList.add("Internal server error.");
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorList);
    }
  }

}