package com.example.demo;

import com.example.demo.models.User;
import com.example.demo.service.SnackService;
import com.example.demo.service.UserService;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ShoppingListController {

  private static final String VIEW_NAME = "shoppinglist";
  private static final String SHOPPING_LIST = "shoppingList";
  private static final String VISITOR = "visitor";
  private static final String API_EXCEPTION = "apiException";
  @Autowired
  private UserService userService;
  @Autowired
  private SnackService snackService;

  @GetMapping("/shoppinglist")
  public ModelAndView shoppinglist(HttpServletResponse response,
                                   @CookieValue(value = VISITOR, required = false) String uuid) {
    User currentUser = userService.getOrCreateUser(uuid);
    response.addCookie(userService.getUserCookie(currentUser));
    ModelAndView modelAndView = new ModelAndView(VIEW_NAME);

    try {
      snackService.syncSnacks();
      modelAndView.addObject(API_EXCEPTION, false);
    } catch (Exception e) {
      modelAndView.addObject(API_EXCEPTION, true);
    }
    modelAndView.addObject(SHOPPING_LIST, snackService.getAllSnacks());
    return modelAndView;
  }

}