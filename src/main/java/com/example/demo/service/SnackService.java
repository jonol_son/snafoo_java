package com.example.demo.service;

import com.example.demo.error.MaxNumberVotesForUserReached;
import com.example.demo.error.SnackHasAlreadyBeenSuggested;
import com.example.demo.error.SnackNotFound;
import com.example.demo.error.SnacksNotFound;
import com.example.demo.error.SuggestedNonOptionalSnack;
import com.example.demo.error.UserHasAlreadyVotedForSnack;
import com.example.demo.helper.DateHelper;
import com.example.demo.models.Snack;
import com.example.demo.models.SnackRepository;
import com.example.demo.models.User;
import com.example.demo.models.Vote;
import com.example.demo.models.VoteRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnackService {

  private static final String SUGGESTED_SNACKS = "Suggested Snacks";
  private static final String ALWAYS_PURCHASED_SNACKS = "Always Purchased Snacks";
  private static final String SUGGESTIBLE_SNACKS = "Snacks Eligible for Suggesting";
  private static final String ALL_SNACKS = "All Snacks";
  @Autowired
  private SnackRepository snackRepository;
  @Autowired
  private VoteRepository voteRepository;
  @Autowired
  private SnackAPIService snackAPIService;

  public void syncSnacks(){
    Snack[] apiSnacks = snackAPIService.fetchSnacks();
    updateSnacksInDB(apiSnacks);
  }

  public void suggestSnack(Snack suggestedSnack){
    Snack existingSnack = snackRepository.findByNameIgnoreCase(suggestedSnack.getName());
    if (existingSnack != null) {
      suggestExistingSnack(existingSnack);
    } else {
      snackAPIService.submitSnack(suggestedSnack);
      suggestNewSnack(suggestedSnack);
    }
  }

  public void suggestExistingSnack(Snack existingSnack) {
    if (!existingSnack.isOptional()) {
      throw new SuggestedNonOptionalSnack(existingSnack.getName());
    }
    if (existingSnack.getSuggestedDate().compareTo(DateHelper.getFirstOfTheMonth()) >= 0) {
      throw new SnackHasAlreadyBeenSuggested(existingSnack.getName());
    }
    existingSnack.suggest();
    snackRepository.save(existingSnack);
  }

  public void suggestNewSnack(Snack suggestedSnack) {
    snackRepository.save(suggestedSnack);
  }

  public void updateSnacksInDB(Snack[] apiSnacks) {
    for (int i = 0; i < apiSnacks.length; i++) {
      saveOrUpdateSnackInDB(apiSnacks[i]);
    }
  }

  public void saveOrUpdateSnackInDB(Snack apiSnack) {
    Snack existingSnack = snackRepository.findByNameIgnoreCase(apiSnack.getName());
    if (existingSnack == null) {
      snackRepository.save(apiSnack);
    } else {
      existingSnack.update(apiSnack);
      snackRepository.save(existingSnack);
    }
  }

  public void userVotesForSnack(User user, Snack snack) {
    List<Vote> votes = voteRepository
        .findByUserAndDateVotedGreaterThanEqual(user, DateHelper.getFirstOfTheMonth());
    if (votes.size() >= 3) {
      throw new MaxNumberVotesForUserReached();
    }

    for (Vote vote : votes) {
      if (vote.getSnackId() == snack.getId()) {
        throw new UserHasAlreadyVotedForSnack(snack.getName());
      }
    }

    snack.vote();
    snackRepository.save(snack);
    Vote currentVote = new Vote(user, snack.getId());
    voteRepository.save(currentVote);
  }

  public Snack findSnackByName(String name) {
    Snack foundSnack = snackRepository.findByNameIgnoreCase(name);
    if (foundSnack == null) {
      throw new SnackNotFound(name);
    }
    return foundSnack;
  }

  public List<Snack> getSuggestedSnacks() {
    List<Snack> foundSnacks = snackRepository
        .findAllBySuggestedDateGreaterThanEqualAndOptional(DateHelper.getFirstOfTheMonth(), true);
    if (foundSnacks.isEmpty()) {
      throw new SnacksNotFound(SUGGESTED_SNACKS);
    }
    return foundSnacks;
  }

  public List<Snack> getAlwaysPurchasedSnacks() {

    List<Snack> foundSnacks = snackRepository.findByOptional(false);
    if (foundSnacks.isEmpty()) {
      throw new SnacksNotFound(ALWAYS_PURCHASED_SNACKS);
    }
    return foundSnacks;
  }

  public List<Snack> getSnacksEligibleForSuggesting() {
    List<Snack> foundSnacks = snackRepository
        .findAllBySuggestedDateLessThanAndOptional(DateHelper.getFirstOfTheMonth(), true);
    if (foundSnacks.isEmpty()) {
      throw new SnacksNotFound(SUGGESTIBLE_SNACKS);
    }
    return foundSnacks;
  }

  public List<Snack> getAllSnacks() {

    List<Snack> foundSnacks = snackRepository.findAll();
    if (foundSnacks.isEmpty()) {
      throw new SnacksNotFound(ALL_SNACKS);
    }
    return foundSnacks;
  }
}
