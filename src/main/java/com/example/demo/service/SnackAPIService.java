package com.example.demo.service;

import com.example.demo.models.Snack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SnackAPIService {

  private final String URL_STRING = "https://api-snacks-staging.nerderylabs.com/v1/snacks?ApiKey=";
  @Value("${ocd.api}")
  private String apiKey;

  @Autowired
  private RestTemplate restTemplate;

  public void submitSnack(Snack suggestSnack) {
    String urlString = URL_STRING + apiKey;
    HttpHeaders headers = new HttpHeaders();

    headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
    HttpEntity<String> entity = new HttpEntity<String>(suggestSnack.toJSON().toString(), headers);
    restTemplate.postForEntity(urlString, entity, String.class);
  }

  public Snack[] fetchSnacks() {
    String urlString = URL_STRING + apiKey;
    ResponseEntity<Snack[]> responseEntity = restTemplate.getForEntity(urlString, Snack[].class);
    Snack[] snacks = responseEntity.getBody();
    return snacks;
  }
}
