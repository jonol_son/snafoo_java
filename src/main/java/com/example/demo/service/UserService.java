package com.example.demo.service;

import com.example.demo.error.UserNotFound;
import com.example.demo.helper.DateHelper;
import com.example.demo.models.User;
import com.example.demo.models.UserRepository;
import com.example.demo.models.Vote;
import com.example.demo.models.VoteRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.Cookie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private static final String VISITOR = "visitor";
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private VoteRepository voteRepository;

  public User getOrCreateUser(String uuid) {
    if (uuid == null) {
      User newUser = new User(UUID.randomUUID().toString());
      newUser = userRepository.save(newUser);
      return newUser;
    }

    User user = userRepository.findByUuid(uuid);
    if (user != null) {
      return user;
    }

    User newUser = new User(UUID.randomUUID().toString());
    newUser = userRepository.save(newUser);
    return newUser;
  }

  public List<Integer> getUsersExistingVotes(User currentUser) {
    List<Integer> existingVotes = new ArrayList<>();
    Date firstOfTheMonth = DateHelper.getFirstOfTheMonth();
    List<Vote> existingVotesObjects = voteRepository
        .findAllByUserAndDateVotedGreaterThanEqual(currentUser, firstOfTheMonth);
    for (Vote vote : existingVotesObjects) {
      existingVotes.add(vote.getSnackId());
    }
    return existingVotes;
  }

  public boolean hasUserSuggested(User currentUser) {
    Date firstOfTheMonth = DateHelper.getFirstOfTheMonth();
    if (currentUser.getLastSuggestedDate() != null
        && currentUser.getLastSuggestedDate().compareTo(firstOfTheMonth) >= 0) {
      return true;
    }
    return false;
  }

  public Cookie getUserCookie(User currentUser) {
    Cookie visitorCookie = new Cookie(VISITOR, currentUser.getUuid());
    visitorCookie.setMaxAge(DateHelper.tenYearsFromNowInSeconds());
    return visitorCookie;
  }

  public void userSuggestedSnack(User currentUser) {
    currentUser.setLastSuggestedDate(new Date());
    userRepository.save(currentUser);
  }

  public User getUserById(Integer userId) {
    User resultingUser = userRepository.findById(userId);
    if (resultingUser == null) {
      throw new UserNotFound();
    }
    return resultingUser;
  }
}
