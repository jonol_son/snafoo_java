package com.example.demo.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = false, nullable = false)
  private Integer id;

  private String name;
  private String uuid;

  private Date lastSuggestedDate;

  public User() {

  }

  public User(String uuid) {
    this.setUuid(uuid);
  }

  public Integer getId() {
    return id;
  }

  private void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  private void setName(String name) {
    this.name = name;
  }

  public String getUuid() {
    return uuid;
  }

  private void setUuid(String uuid) {
    this.uuid = uuid;
  }


  public Date getLastSuggestedDate() {
    return lastSuggestedDate;
  }

  public void setLastSuggestedDate(Date lastSuggestedDate) {
    this.lastSuggestedDate = lastSuggestedDate;
  }
}