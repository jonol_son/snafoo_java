package com.example.demo.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity // This tells Hibernate to make a table out of this class
public class Vote {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = false, nullable = false)
  private Integer id;
  private Integer snackId;
  private Date dateVoted;

  @ManyToOne
  private User user;

  public Vote() {
  }

  public Vote(User user, Integer snackId) {
    this.setUser(user);
    this.setSnackId(snackId);
    this.setDateVoted(new Date());
  }

  public Integer getId() {
    return id;
  }

  private void setId(Integer id) {
    this.id = id;
  }

  public Integer getSnackId() {
    return snackId;
  }

  private void setSnackId(Integer snackId) {
    this.snackId = snackId;
  }

  public Date getDateVoted() {
    return dateVoted;
  }

  private void setDateVoted(Date dateVoted) {
    this.dateVoted = dateVoted;
  }

  public User getUser() {
    return user;
  }

  private void setUser(User user) {
    this.user = user;
  }
}