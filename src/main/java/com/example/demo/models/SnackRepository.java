package com.example.demo.models;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface SnackRepository extends JpaRepository<Snack, Long> {

  Snack findById(Integer id);

  Snack findByNameIgnoreCase(String name);

  List<Snack> findByOptional(boolean optional);

  List<Snack> findAllBySuggestedDateGreaterThanEqualAndOptional(Date endDate, boolean optional);

  List<Snack> findAllBySuggestedDateLessThanAndOptional(Date endDate, boolean optional);

}