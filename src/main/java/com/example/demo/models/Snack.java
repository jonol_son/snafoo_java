package com.example.demo.models;

import com.example.demo.helper.DateHelper;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import org.json.JSONObject;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity // This tells Hibernate to make a table out of this class
public class Snack {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = true, nullable = false)
  private Integer id;

  @Size(min = 2, max = 200, message = "Name should be between 2 and 200 characters")
  @Column(unique = true)
  @JsonProperty
  private String name;

  @JsonProperty
  private boolean optional;

  @JsonProperty
  private String purchaseLocations;

  @JsonProperty
  private Integer purchaseCount;

  @JsonProperty
  private Date lastPurchasedDate;

  @Column(name = "votes", nullable = false, columnDefinition = "int default 0")
  private Integer votes;

  private Date suggestedDate;

  public Snack() {
  }

  @JsonCreator
  public Snack(@JsonProperty("name") final String name,
               @JsonProperty("optional") final Boolean optional,
               @JsonProperty("lastPurchaseDate") final String lastPurchaseDate)
      throws ParseException {
    this.name = name;
    this.optional = optional;
    this.setLastPurchasedDate(lastPurchaseDate);
    this.setVotes(0);
    this.setSuggestedDate(DateHelper.getLastMonth());
  }

  public Snack(String name, String purchaseLocations) {
    this.setName(name);
    this.setPurchaseLocations(purchaseLocations);
    this.setSuggestedDate(new Date());
    this.setOptional(true);
    this.setVotes(0);
  }

  public Snack(String name, String purchaseLocations, Date suggestedDate, Boolean isOptional, Integer votes, Date lastPurchasedDate, Integer purchaseCount) {
    this.setName(name);
    this.setPurchaseLocations(purchaseLocations);
    this.setSuggestedDate(suggestedDate);
    this.setOptional(isOptional);
    this.setVotes(votes);
    this.setLastPurchasedDate(lastPurchasedDate);
    this.setPurchaseCount(purchaseCount);
  }

  public void update(Snack snack) {
    this.purchaseLocations = snack.getPurchaseLocations();
    this.purchaseCount = snack.getPurchaseCount();
    this.optional = snack.isOptional();
    this.lastPurchasedDate = snack.getLastPurchasedDate();
  }

  public Integer getId() {
    return id;
  }

  private void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  private void setName(String name) {
    this.name = name;
  }

  public boolean isOptional() {
    return optional;
  }

  private void setOptional(boolean optional) {
    this.optional = optional;
  }

  public String getPurchaseLocations() {
    return purchaseLocations;
  }

  private void setPurchaseLocations(String purchaseLocations) {
    this.purchaseLocations = purchaseLocations;
  }

  public Integer getPurchaseCount() {
    return purchaseCount;
  }

  private void setPurchaseCount(Integer purchaseCount) {
    this.purchaseCount = purchaseCount;
  }

  public Date getLastPurchasedDate() {
    return lastPurchasedDate;
  }

  private void setLastPurchasedDate(String lastPurchasedDate) throws ParseException {
    DateFormat dateFormat = new SimpleDateFormat(DateHelper.getMonthDateYearString());
    Date startDate;
    if (lastPurchasedDate != null) {
      startDate = dateFormat.parse(lastPurchasedDate);
      this.lastPurchasedDate = startDate;
    } else {
      this.lastPurchasedDate = null;
    }
  }

  private void setLastPurchasedDate(Date lastPurchasedDate) {
    this.lastPurchasedDate = lastPurchasedDate;
  }

  public Integer getVotes() {
    return votes;
  }

  private void setVotes(Integer votes) {
    this.votes = votes;
  }

  public Date getSuggestedDate() {
    return suggestedDate;
  }

  private void setSuggestedDate(Date suggestedDate) {
    this.suggestedDate = suggestedDate;
  }

  public void vote() {
    this.setVotes(++this.votes);
  }

  public void suggest() {
    this.setSuggestedDate(new Date());
  }

  public JSONObject toJSON() {
    JSONObject snackJSON = new JSONObject();
    snackJSON.put("name", this.getName());
    snackJSON.put("location", this.getPurchaseLocations());
    return snackJSON;
  }
}