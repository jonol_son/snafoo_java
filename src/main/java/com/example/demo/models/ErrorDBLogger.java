package com.example.demo.models;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class ErrorDBLogger {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = false, nullable = false)
  private Integer id;

  private Date date;

  private String message;

  private String errorType;

  private String stackTrace;

  public ErrorDBLogger(Exception e) {
    StringWriter stringWriter = new StringWriter();
    PrintWriter printWriter = new PrintWriter(stringWriter);
    e.printStackTrace(printWriter);
    String stackTrace = stringWriter.toString();

    setDate(new Date());
    setMessage(e.getMessage());
    setStackTrace(stackTrace);
    setErrorType(e.toString());
  }

  private void setDate(Date date) {
    this.date = date;
  }

  private void setMessage(String message) {
    this.message = message;
  }

  private void setStackTrace(String stackTrace) {
    if (stackTrace.length() <= 255) {
      this.stackTrace = stackTrace;
    } else {
      this.stackTrace = stackTrace.substring(0, 255);
    }
  }

  private void setErrorType(String errorType) {
    this.errorType = errorType;
  }
}
