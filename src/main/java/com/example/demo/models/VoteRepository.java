package com.example.demo.models;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface VoteRepository extends JpaRepository<Vote, Long> {

  List<Vote> findByUserAndDateVotedGreaterThanEqual(User user, Date date);

  List<Vote> findAll();

  List<Vote> findAllByUserAndDateVotedGreaterThanEqual(User user, Date date);
}