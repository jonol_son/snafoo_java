package com.example.demo.helper;

import java.time.LocalDate;
import java.util.Date;

public class DateHelper {

  public static Date getFirstOfTheMonth() {
    LocalDate today = LocalDate.now();
    return java.sql.Date.valueOf(today.withDayOfMonth(1));
  }

  public static Date getLastMonth() {
    LocalDate today = LocalDate.now();
    return java.sql.Date.valueOf(today.minusMonths(1));
  }

  public static int tenYearsFromNowInSeconds() {
    return 60 * 60 * 24 * 365 * 10;
  }

  public static String getMonthDateYearString() {
    return "MM/dd/yyyy";
  }
}
