package com.example.demo.forms;

import com.example.demo.models.Snack;
import javax.validation.constraints.Size;
import org.json.JSONObject;

public class SuggestSnackForm {

  @Size(min = 2, max = 200, message = "Name should be between 2 and 200 characters")
  private String name;

  @Size(min = 2, max = 200, message = "Purchase Location should be between 2 and 200 characters")
  private String purchaseLocation;


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPurchaseLocation() {
    return purchaseLocation;
  }

  public void setPurchaseLocation(String purchaseLocation) {
    this.purchaseLocation = purchaseLocation;
  }

  public Snack getSnack() {
    Snack snackObject = new Snack(this.getName(), this.getPurchaseLocation());
    return snackObject;
  }
}
