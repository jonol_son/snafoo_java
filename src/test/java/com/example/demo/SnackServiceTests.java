package com.example.demo;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.example.demo.error.MaxNumberVotesForUserReached;
import com.example.demo.error.SnackNotFound;
import com.example.demo.error.SnacksNotFound;
import com.example.demo.error.UserHasAlreadyVotedForSnack;
import com.example.demo.helper.DateHelper;
import com.example.demo.models.Snack;
import com.example.demo.models.User;
import java.util.Date;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SnackServiceTests {

  private final String INVALID_SNACK_NAME = "G";
  private final String SNACK_NAME = "Cookie";
  private final String SNACK_PURCHASE_LOCATION = "Cub";
  private final Date CURRENT_DATE = new Date();
  private final Date LAST_MONTH = DateHelper.getLastMonth();
  private final boolean IS_OPTIONAL = true;
  private final Integer THREE = 3;
  private final String SNACK_NAME_2 = "Apple";
  private final String SNACK_PURCHASE_LOCATION_2 = "Fresh Thyme";
  private final String SNACK_NAME_3 = "Peanuts";
  private final String SNACK_PURCHASE_LOCATION_3 = "Target";
  private final String SNACK_NAME_4 = "RedBull";
  private final String SNACK_PURCHASE_LOCATION_4 = "Corner Store";

  @Autowired
  protected com.example.demo.service.SnackService snackService;
  @Autowired
  protected com.example.demo.service.UserService userService;
  @Autowired
  protected com.example.demo.models.SnackRepository snackRepository;

  @Test
  @DisplayName("Find a Snack by invalid name throws SnackNotFound exception")
  public void findSnackByInvalidName() {
    assertThrows(
        SnackNotFound.class, () -> {
          snackService.findSnackByName(INVALID_SNACK_NAME);
        });
  }

  @Test
  @DisplayName("Find a Snack by name returns snack")
  public void findSnackByName() {
    Snack newSnack = new Snack(SNACK_NAME, SNACK_PURCHASE_LOCATION);
    snackService.saveOrUpdateSnackInDB(newSnack);
    Snack foundSnack = snackService.findSnackByName(SNACK_NAME);
    assertEquals(newSnack.getName(), foundSnack.getName());
    assertEquals(foundSnack.getPurchaseLocations(), SNACK_PURCHASE_LOCATION);
    snackRepository.delete(foundSnack);
  }

  @Test
  @DisplayName("Find Suggested Snacks when none exist throws SnacksNotFound exception")
  public void findSuggestedSnacksWithNone() {
    assertThrows(
        SnacksNotFound.class, () -> {
          snackService.getSuggestedSnacks();
        });
  }

  @Test
  @DisplayName("Find Always Purchased Snacks when none exist throws SnacksNotFound exception")
  public void findAlwaysPurchasedSnackWithNone() {
    assertThrows(
        SnacksNotFound.class, () -> {
          snackService.getAlwaysPurchasedSnacks();
        });
  }

  @Test
  @DisplayName("Find Snacks Eligible For Suggesting when none exist throws SnacksNotFound exception")
  public void findSnacksEligibleForSuggestingSnackWithNone() {
    assertThrows(
        SnacksNotFound.class, () -> {
          snackService.getSnacksEligibleForSuggesting();
        });
  }

  @Test
  @DisplayName("Find All Snacks when none exist throws SnacksNotFound exception")
  public void findAllSnacksWithNone() {
    assertThrows(
        SnacksNotFound.class, () -> {
          snackService.getAllSnacks();
        });
  }

  @Test
  @DisplayName("Save or Update saves new snack to database")
  public void saveOrUpdateSavesNewSnack() {
    Snack newSnack = new Snack(SNACK_NAME, SNACK_PURCHASE_LOCATION);
    snackService.saveOrUpdateSnackInDB(newSnack);
    Snack foundSnack = snackService.findSnackByName(SNACK_NAME);
    assertEquals(newSnack.getName(), foundSnack.getName());
    assertEquals(newSnack.getPurchaseLocations(), foundSnack.getPurchaseLocations());
    snackRepository.delete(foundSnack);
  }

  @Test
  @DisplayName("Save or Update updates snack in database")
  public void saveOrUpdateUpdatesSnack() {
    Snack newSnack = new Snack(SNACK_NAME, SNACK_PURCHASE_LOCATION);
    snackService.saveOrUpdateSnackInDB(newSnack);
    Snack newSnack2 = new Snack(
        SNACK_NAME, SNACK_PURCHASE_LOCATION_2, CURRENT_DATE, IS_OPTIONAL, THREE, CURRENT_DATE,
        THREE
    );
    snackService.saveOrUpdateSnackInDB(newSnack2);
    Snack foundSnack = snackService.findSnackByName(SNACK_NAME);
    assertEquals(newSnack.getName(), foundSnack.getName());
    assertEquals(foundSnack.getPurchaseLocations(), SNACK_PURCHASE_LOCATION_2);
    assertEquals(foundSnack.getPurchaseCount(), THREE);
    assertEquals(foundSnack.isOptional(), IS_OPTIONAL);
    assertNotNull(foundSnack.getLastPurchasedDate());
    snackRepository.delete(foundSnack);
  }

  @Test
  @DisplayName("User voting for snack already voted for throws UserHasAlreadyVotedForSnack exception")
  public void userVotingForSameSnackTwiceThrowsException() {
    Snack newSnack = new Snack(SNACK_NAME, SNACK_PURCHASE_LOCATION);
    snackService.saveOrUpdateSnackInDB(newSnack);
    User newUser = userService.getOrCreateUser(null);
    snackService.userVotesForSnack(newUser, newSnack);
    assertThrows(
        UserHasAlreadyVotedForSnack.class, () -> {
          snackService.userVotesForSnack(newUser, newSnack);
        });
    snackRepository.delete(newSnack);
  }

  @Test
  @DisplayName("User voting for 4th snack throws MaxNumberVotesForUserReached exception")
  public void userVotingFourthTimeThrowsException() {
    Snack newSnack = new Snack(SNACK_NAME, SNACK_PURCHASE_LOCATION);
    snackService.saveOrUpdateSnackInDB(newSnack);
    Snack newSnack2 = new Snack(SNACK_NAME_2, SNACK_PURCHASE_LOCATION_2);
    snackService.saveOrUpdateSnackInDB(newSnack2);
    Snack newSnack3 = new Snack(SNACK_NAME_3, SNACK_PURCHASE_LOCATION_3);
    snackService.saveOrUpdateSnackInDB(newSnack3);
    Snack newSnack4 = new Snack(SNACK_NAME_4, SNACK_PURCHASE_LOCATION_4);
    snackService.saveOrUpdateSnackInDB(newSnack4);
    User newUser = userService.getOrCreateUser(null);
    snackService.userVotesForSnack(newUser, newSnack);
    snackService.userVotesForSnack(newUser, newSnack2);
    snackService.userVotesForSnack(newUser, newSnack3);
    assertThrows(
        MaxNumberVotesForUserReached.class, () -> {
          snackService.userVotesForSnack(newUser, newSnack4);
        });
    snackRepository.delete(newSnack);
    snackRepository.delete(newSnack2);
    snackRepository.delete(newSnack3);
    snackRepository.delete(newSnack4);
  }
}
