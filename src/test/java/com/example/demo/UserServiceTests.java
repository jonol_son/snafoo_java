package com.example.demo;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.example.demo.error.UserNotFound;
import com.example.demo.helper.DateHelper;
import com.example.demo.models.Snack;
import com.example.demo.models.SnackRepository;
import com.example.demo.models.User;
import com.example.demo.models.UserRepository;
import com.example.demo.service.SnackService;
import com.example.demo.service.UserService;
import java.util.List;
import javax.servlet.http.Cookie;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTests {

  private final String SNACK_NAME = "Cookie";
  private final String SNACK_PURCHASE_LOCATION = "Cub";

  @Autowired
  protected UserRepository userRepository;
  @Autowired
  protected SnackRepository snackRepository;
  @Autowired
  private UserService userService;
  @Autowired
  private SnackService snackService;

  @Test
  @DisplayName("New User is created if UUID is null")
  public void createsNewUserIfUUIDIsNull() {
    User newUser = userService.getOrCreateUser(null);
    assertNotNull(newUser);
    assertNotNull(newUser.getUuid());
    assertNull(newUser.getLastSuggestedDate());
  }

  @Test
  @DisplayName("User UUID is retrieved if UUID exists")
  public void retrievesUserIfUUIDIsNotNull() {
    User newUser = userService.getOrCreateUser(null);
    String existingUUID = newUser.getUuid();
    User existingUser = userService.getOrCreateUser(existingUUID);
    assertNotNull(existingUser);
    assertNotNull(existingUser.getUuid());
    assertEquals(existingUUID, existingUser.getUuid());
    assertNull(existingUser.getLastSuggestedDate());
  }

  @Test
  @DisplayName("User's existing votes can be retrieved")
  public void retrievesUsersExistingVotes() {
    User newUser = userService.getOrCreateUser(null);
    Snack snackToVoteFor = new Snack(SNACK_NAME, SNACK_PURCHASE_LOCATION);
    snackService.saveOrUpdateSnackInDB(snackToVoteFor);
    snackService.userVotesForSnack(newUser, snackToVoteFor);
    List<Integer> votes = userService.getUsersExistingVotes(newUser);
    assertNotNull(votes);
    assertEquals(1, votes.size());
    assertEquals(snackToVoteFor.getId(), votes.get(0));
  }

  @Test
  @DisplayName("New User hasSuggested returns false")
  public void newUserHasSuggestedIsFalse() {
    User newUser = userService.getOrCreateUser(null);
    assertEquals(false, userService.hasUserSuggested(newUser));
  }

  @Test
  @DisplayName("User which has suggested a snack shows hasSuggested as True")
  public void userHasSuggestedReturnsTrue() {
    User newUser = userService.getOrCreateUser(null);
    userService.userSuggestedSnack(newUser);
    assertEquals(true, userService.hasUserSuggested(newUser));
  }

  @Test
  @DisplayName("Get User By invalid Id throws UserNotFound exception")
  public void getUserByInvalidIdThrowsUserNotFoundException() {
    assertThrows(
        UserNotFound.class, () -> {
          userService.getUserById(1231231231);
        });
  }

  @Test
  @DisplayName("Get User By Id returns user")
  public void getUserByIdReturnsId() {
    User newUser = userService.getOrCreateUser(null);
    User retrievedUser = userService.getUserById(newUser.getId());
    assertEquals(retrievedUser.getUuid(), newUser.getUuid());
    assertEquals(retrievedUser.getName(), newUser.getName());
    assertEquals(retrievedUser.getLastSuggestedDate(), newUser.getLastSuggestedDate());
    assertEquals(retrievedUser.getId(), newUser.getId());
  }

  @Test
  @DisplayName("Get User Cookie returns cookie with max age set to ten years")
  public void getUserCookieReturnsCookie() {
    User newUser = userService.getOrCreateUser(null);
    Cookie userCookie = userService.getUserCookie(newUser);
    assertNotNull(userCookie);
    assertEquals(userCookie.getMaxAge(), DateHelper.tenYearsFromNowInSeconds());
  }
}
